import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { NgxCarouselModule } from 'projects/ngx-light-carousel/src/public-api'
import { NgxFaderModule } from 'projects/ngx-light-fader/src/public-api'
import { AppComponent } from './app.component'

@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule, NgxCarouselModule, NgxFaderModule],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
