import { Component } from '@angular/core'
import { ngxLightOptions } from 'projects/ngx-light-carousel/src/public-api'
import { ngxLightFaderOptions } from 'projects/ngx-light-fader/src/public-api'

@Component({
	selector: 'ngx-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	current: number = 0
	products: any
	products2: any
	options1: ngxLightFaderOptions
	options2: ngxLightOptions
	options3: ngxLightOptions
	button: boolean
	constructor() {
		this.options1 = {
			animation: {
				animationClass: 'transition',
				animationTime: 200,
			},
			swipe: {
				swipeable: true,
				swipeVelocity: 0.005,
			},
			infinite: false,
			autoplay: {
				enabled: true,
				direction: 'right',
				delay: 2000,
				stopOnHover: true,
			},
			arrows: true,
			indicators: true,
			vertical: true,
		}
		this.options2 = {
			animation: {
				animationClass: 'transition', // done
				animationTime: 200,
			},
			swipe: {
				swipeable: true, // done
				swipeVelocity: 0.005, // done - check amount
			},
			drag: {
				draggable: true, // done
				dragMany: true, // todo
			},
			infinite: true,
			autoplay: {
				enabled: true,
				direction: 'right',
				delay: 1000,
				stopOnHover: true,
			},
			scroll: {
				numberToScroll: 1,
			},
			arrows: true,
			breakpoints: [
				{
					width: 450,
					number: 1.2,
				},
				{
					width: 991,
					number: 2.5,
				},
				{
					width: 9999,
					number: 4,
				},
			],
		}
		this.options3 = {
			animation: {
				animationClass: 'transition', // done
				animationTime: 200,
			},
			swipe: {
				swipeable: true, // done
				swipeVelocity: 0.0003, // done - check amount
			},
			scroll: {
				numberToScroll: 1,
			},
			drag: {
				draggable: true, // done
				dragMany: true, // todo
			},
			autoplay: {
				enabled: false,
				direction: 'right',
				delay: 1000,
				stopOnHover: true,
			},
			infinite: false,
			breakpoints: [
				{
					width: 768,
					number: 1.3,
				},
				{
					width: 991,
					number: 3,
				},
				{
					width: 9999,
					number: 4,
				},
			],
		}
		this.products = []
		this.products.push({
			title: 'RED Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			salePrice: '100.00',
			image: `http://picsum.photos/600/400/?0`,
		})
		this.products.push({
			title: 'YELLOW Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			salePrice: '100.00',
			image: `http://picsum.photos/600/400/?1`,
		})
		this.products.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			salePrice: '100.00',
			image: `http://picsum.photos/600/400/?2`,
		})
		this.products.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?3`,
		})
		this.products.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?4`,
		})
		this.products.push({
			title: 'YELLOW Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			salePrice: '100.00',
			image: `http://picsum.photos/600/400/?1`,
		})
		this.products.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			salePrice: '100.00',
			image: `http://picsum.photos/600/400/?2`,
		})
		this.products.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?3`,
		})
		this.products.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?4`,
		})
		this.products2 = []
		this.products2.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?5`,
		})
		this.products2.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?6`,
		})
		this.products2.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?7`,
		})
		this.products2.push({
			title: 'Black Widgets',
			url: 'https://url',
			regularPrice: '100.00',
			image: `http://picsum.photos/600/400/?8`,
		})
	}
}
