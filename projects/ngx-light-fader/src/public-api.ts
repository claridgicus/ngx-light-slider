/*
 * Public API Surface of ngx-light-carousel
 */

export * from './lib/ngx-light-fader-item.directive'
export * from './lib/ngx-light-fader.component'
export * from './lib/ngx-light-fader.model'
export * from './lib/ngx-light-fader.module'
