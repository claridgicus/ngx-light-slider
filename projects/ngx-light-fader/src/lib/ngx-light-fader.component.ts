import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ContentChild,
	ContentChildren,
	DoCheck,
	ElementRef,
	EmbeddedViewRef,
	EventEmitter,
	HostListener,
	Inject,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	PLATFORM_ID,
	QueryList,
	Renderer2,
	TemplateRef,
	ViewChild,
	ViewContainerRef,
	ViewEncapsulation,
} from '@angular/core'
import * as Hammer from 'hammerjs'
import { interval, Observable, Subject, Subscription } from 'rxjs'
import { throttle } from 'rxjs/operators'
import { NgxFaderItemDirective } from './ngx-light-fader-item.directive'
import { ngxLightFaderAutoplay, ngxLightFaderOptions } from './ngx-light-fader.model'
@Component({
	selector: 'ngx-light-fader',
	templateUrl: './ngx-light-fader.component.html',
	styleUrls: ['./ngx-light-fader.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None,
})
export class NgxFaderComponent implements AfterViewInit, OnChanges, OnInit, OnDestroy, DoCheck {
	@ViewChild('faderElement') faderElement: ElementRef
	@ViewChild('prevElement') btnPrevElement: ElementRef
	@ViewChild('nextElement') btnNextElement: ElementRef
	@ViewChild('progressElement') progressContainerElement: ElementRef
	@ContentChildren(NgxFaderItemDirective, { read: ElementRef, descendants: true }) faderItems: QueryList<ElementRef>
	@ContentChild('faderPrev') contentPrev: TemplateRef<any>
	@ContentChild('faderNext') contentNext: TemplateRef<any>
	@ContentChild('faderDot') dotElm: TemplateRef<any>
	@ContentChild('faderProgress') progressElm: TemplateRef<any>
	@ContentChild('infiniteContainer', { static: false, read: ViewContainerRef }) infiniteContainer: ViewContainerRef
	@ContentChild('faderContent') contentContent: TemplateRef<any>
	@Input() current: number = 0
	@Output() currentChange: EventEmitter<number> = new EventEmitter()
	@Input() data: any[]
	@Input() options: ngxLightFaderOptions
	private infiniteElmRefs: Array<EmbeddedViewRef<any>> = []
	private timeLeft: number = 2000
	private interval
	private actionDelay: Subject<any>
	dots: number[]
	dotsLoading: boolean = true
	loading: boolean = true
	grabbing: boolean = false
	throttleSub: Observable<any>
	subRef: Subscription
	panToTimeout
	constructor(@Inject(PLATFORM_ID) private platformId: Object, private _renderer: Renderer2, private cd: ChangeDetectorRef) {
		this.loading = true
		this.actionDelay = new Subject<any>()
		this.throttleSub = this.actionDelay.asObservable()
		this.subRef = this.throttleSub.pipe(throttle(val => interval(this.options.animation.animationTime + 40))).subscribe(data => {
			data()
		})
	}

	click(fn) {
		this.actionDelay.next(fn)
	}

	ngOnInit() {}

	ngAfterViewInit() {
		this.setup()
		this.dots = []
		console.log(this.current, '<<<<<<<<<<<<<<<<<<<<<<')

		this.calculateDots()
	}

	ngOnChanges() {
		this.setViewWidth()
	}

	ngDoCheck() {
		// console.log('checking')
	}

	ngOnDestroy() {
		this.pauseTimer()
		clearTimeout(this.panToTimeout)
		this.subRef.unsubscribe()
	}

	@HostListener('window:resize')
	onResize() {
		this.setViewWidth()
		this.calculateDots()
		this.pauseTimer()

		this.goToIndex(this.currentIndex())
	}

	setup() {
		this.init()
		this.onResize()
		this.loading = false
		// if (this.autoplayMode()) {
		// 	this.startTimer()
		// }
	}

	rootElmWidth() {
		return this.elements().rootElement.offsetWidth
	}

	elements() {
		if (this.faderElement) {
			return {
				rootElement: this.faderElement.nativeElement,
				containerElement: this.faderElement.nativeElement.children[0] as HTMLElement,
				faderItems: this.faderItems.toArray().map(x => x.nativeElement),
			}
		}
		return
	}

	private setViewWidth() {
		if (this.elements() && this.elements().faderItems) {
			this._renderer.setStyle(this.elements().faderItems[0], 'width', this.rootElmWidth() + 'px')
			this._renderer.setStyle(this.elements().containerElement, 'display', 'block')
			this.elements().faderItems.forEach((element: HTMLElement) => {
				this._renderer.addClass(element, 'slide')
				this._renderer.setStyle(element, 'width', this.rootElmWidth() + 'px')
				this._renderer.setStyle(element, 'right', this.elements().faderItems[0].offsetWidth + 'px')
				this._renderer.setStyle(element, 'display', 'block')
				this._renderer.setStyle(element, 'left', '0px')
				this._renderer.setStyle(element, 'top', '0px')
				this._renderer.setStyle(element, 'z-index', '998')
				this._renderer.setStyle(element, 'right', this.rootElmWidth() + 'px')
			})
		}
	}

	grabbingState(state?: boolean) {
		this.grabbing = state
		return state
	}

	@HostListener('mouseover')
	hover() {
		if (this.autoplayMode()) {
			if ((this.autoplayMode() as any).pauseOnHover) {
				this.pauseTimer()
			}
		}
	}

	unhover() {
		if (this.autoplayMode()) {
			if ((this.autoplayMode() as any).pauseOnHover) {
				this.startTimer()
			}
		}
	}

	private calculateDots() {
		this.dots = []
		var number = this.data.length

		if (number > 1) {
			let results = Array.from(Array(Number(number.toFixed(0))).keys())
			this.dots = results
			this.dotsLoading = false
			return results
		} else {
			let results = []
			this.dots = []
			this.dotsLoading = false
			return results
		}
	}

	addPanningClass = () => {
		this.grabbing = true
		this._renderer.addClass(this.elements().containerElement, 'panning')
	}

	removePanningClass = () => {
		this.grabbing = false
		this._renderer.removeClass(this.elements().containerElement, 'panning')
	}

	addAnimationClass = () => {
		if (this.options.animation.animationClass !== '') {
			this._renderer.addClass(this.elements().containerElement, this.options.animation.animationClass)
		}
	}

	removeAnimationClass = () => {
		if (this.options.animation.animationClass !== '') {
			this._renderer.removeClass(this.elements().containerElement, this.options.animation.animationClass)
		}
	}

	panTo = (value: number) => {
		for (let i = 0; i < this.elements().faderItems.length; i++) {
			if (i === value) {
				continue
			}
			this.panToTimeout = setTimeout(() => {
				this._renderer.removeClass(this.elements().faderItems[i], 'visible')
			}, this.options.animation.animationTime)

			this._renderer.setStyle(this.elements().faderItems[i], 'position', 'absolute')
			this._renderer.setStyle(this.elements().faderItems[i], 'z-index', '998')
		}
		this._renderer.setStyle(this.elements().faderItems[value], 'position', 'relative')
		this._renderer.setStyle(this.elements().faderItems[value], 'z-index', '999')
		this._renderer.addClass(this.elements().faderItems[value], 'visible')
	}

	currentIndex = (number?: number) => {
		if (!this.infiniteMode() && number !== undefined) {
			if (number >= 0) {
				this.current = number
			}
		} else if (number != undefined) {
			this.current = number
		}
		return this.current
	}

	detectSwipingLeft = e => {
		return e.velocityX < -this.options.swipe.swipeVelocity
	}

	detectSwipingRight = e => {
		return e.velocityX > this.options.swipe.swipeVelocity
	}

	detectSwipingUp = e => {
		return e.velocityY < -this.options.swipe.swipeVelocity
	}

	detectSwipingDown = e => {
		return e.velocityY > this.options.swipe.swipeVelocity
	}

	nextSlide = () => {
		console.log(this.currentIndex())
		this.addAnimationClass()
		if (this.data.length > 1) {
			if (!this.infiniteMode() && this.data.length < this.currentIndex()) {
				this.goToIndex(this.data.length)
			} else if (this.data.length - 1 == this.currentIndex()) {
				this.goToIndex(this.currentIndex(0))
			} else {
				this.goToIndex(this.currentIndex() + 1)
			}
			this.infiniteHandler()

			this.goToIndex(this.currentIndex())
		}
	}

	prevSlide = () => {
		console.log(this.currentIndex())
		this.addAnimationClass()
		if (this.data.length > 1) {
			if (!this.infiniteMode() && 0 >= this.currentIndex()) {
				this.goToIndex(this.currentIndex(0))
			} else if (0 >= this.currentIndex()) {
				this.goToIndex(this.currentIndex(this.data.length - 1))
			} else {
				this.goToIndex(this.currentIndex() - 1)
			}
			this.infiniteHandler()

			this.goToIndex(this.currentIndex())
		}
	}

	private infiniteHandler() {
		this.addAnimationClass()
		this.goToIndex(this.currentIndex())
		if (0 > this.currentIndex()) {
			this.currentIndex(this.data.length - 1)

			setTimeout(() => {
				this.removeAnimationClass()
				this.goToIndex(this.currentIndex(this.data.length - 1))
			}, this.options.animation.animationTime + 10)
		} else if (this.data.length - 1 < this.currentIndex()) {
			this.currentIndex(0)

			setTimeout(() => {
				this.removeAnimationClass()
				this.goToIndex(this.currentIndex(0))
			}, this.options.animation.animationTime + 10)
		}
		return
	}

	scrollToIndex(number) {
		this.addAnimationClass()
		this.goToIndex(number)
	}

	goToIndex = number => {
		if (this.infiniteMode()) {
			this.panTo(number)
		} else {
			if (number > -1 && number <= this.data.length - 1) {
				this.currentIndex(number)
				this.panTo(number)
			}
			if (number <= -1) {
				this.currentIndex(0)
				this.goToIndex(0)
			}
		}
		this.currentIndex(number)
	}

	infiniteMode() {
		return this.options.infinite
	}

	autoplayMode() {
		if (this.options.autoplay && this.options.autoplay.enabled) {
			return this.options.autoplay
		} else {
			return false
		}
	}

	startTimer() {
		var autoplay = this.autoplayMode() as ngxLightFaderAutoplay
		if (autoplay) {
			this.interval = setInterval(() => {
				if (this.timeLeft > 0) {
					this.timeLeft = this.timeLeft - 100
				} else {
					this.timeLeft = JSON.parse(JSON.stringify(autoplay.delay))
					this.actionDelay.next(autoplay.direction == 'left' ? this.prevSlide : this.nextSlide)
				}
			}, 100)
		}
	}

	pauseTimer() {
		var autoplay = this.autoplayMode() as ngxLightFaderAutoplay
		this.timeLeft = JSON.parse(JSON.stringify(autoplay.delay))
		clearInterval(this.interval)
		delete this.interval
	}

	init() {
		if (this.data.length > 1) {
			var faderManager = new Hammer.Manager(this.elements().containerElement)
			faderManager.add(new Hammer.Pan({ threshold: 0, pointers: 0 }))
			faderManager.on('panup pandown panleft panright panend pancancel', (e: Hammer.HammerInput) => {
				switch (e.type) {
					case 'panleft':
					case 'panright':
					case 'panup':
					case 'pandown':
						this.removeAnimationClass()
						this.addPanningClass()
						this.pauseTimer()
						e.preventDefault()
						break
					case 'pancancel':
						this.pauseTimer()
						e.preventDefault()
						break
					case 'panend':
						this.removePanningClass()
						this.addAnimationClass()
						if (this.options.vertical) {
							if (this.detectSwipingUp(e)) {
								this.nextSlide()
								break
							}
							if (this.detectSwipingDown(e)) {
								this.prevSlide()
								break
							}
						}
						if (this.options.swipe.swipeable) {
							if (this.detectSwipingLeft(e)) {
								this.nextSlide()
								break
							}
							if (this.detectSwipingRight(e)) {
								this.prevSlide()
								break
							}
						}
						if (this.autoplayMode()) {
							this.startTimer()
						}
						this.goToIndex(this.currentIndex())
				}
			})
			return faderManager
		}
	}
}
