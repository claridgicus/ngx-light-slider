export class ngxLightFaderOptions {
	animation?: {
		animationClass: string
		animationTime: number
	} | null
	swipe?: {
		swipeable: boolean
		swipeVelocity: number
	} | null
	infinite?: boolean
	indicators?: boolean = true
	arrows?: boolean = true
	autoplay?: ngxLightFaderAutoplay | null
	vertical?: boolean = false
}

export class ngxLightFaderAutoplay {
	enabled: boolean
	direction: 'left' | 'right'
	delay: number
	stopOnHover: boolean
}
