import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { NgxFaderItemDirective } from './ngx-light-fader-item.directive'
import { NgxFaderComponent } from './ngx-light-fader.component'

@NgModule({
	imports: [CommonModule, FormsModule],
	declarations: [NgxFaderComponent, NgxFaderItemDirective],
	exports: [NgxFaderComponent, NgxFaderItemDirective],
})
export class NgxFaderModule {}
