/*
 * Public API Surface of ngx-light-carousel
 */

export * from './lib/ngx-light-carousel.component'
export * from './lib/ngx-light-carousel.module'
export * from './lib/ngx-light-carousel.model'
export * from './lib/ngx-light-carousel-item.directive'
