import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { NgxCarouselItemDirective } from './ngx-light-carousel-item.directive'
import { NgxCarouselComponent } from './ngx-light-carousel.component'

@NgModule({
	imports: [CommonModule, FormsModule],
	declarations: [NgxCarouselComponent, NgxCarouselItemDirective],
	exports: [NgxCarouselComponent, NgxCarouselItemDirective],
})
export class NgxCarouselModule {}
