export class ngxLightOptions {
	animation?: {
		animationClass: string
		animationTime: number
	} | null
	swipe?: {
		swipeable: boolean
		swipeVelocity: number
	} | null
	drag?: {
		draggable: boolean
		dragMany: boolean
	}
	scroll: {
		numberToScroll: number
	} | null
	indicators?: boolean = true
	arrows?: boolean = true
	infinite?: boolean
	autoplay?: ngxLightAutoplay | null
	breakpoints: ngxLightBreakpoint[]
}
export class ngxLightBreakpoint {
	width: number
	number: number
}
export class ngxLightAutoplay {
	enabled: boolean
	direction: 'left' | 'right'
	delay: number
	stopOnHover: boolean
}
