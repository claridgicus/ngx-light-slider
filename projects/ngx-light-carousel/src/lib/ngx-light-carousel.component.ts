import { isPlatformBrowser } from '@angular/common'
import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ContentChild,
	ContentChildren,
	ElementRef,
	EmbeddedViewRef,
	EventEmitter,
	HostListener,
	Inject,
	Input,
	OnChanges,
	OnDestroy,
	Output,
	PLATFORM_ID,
	QueryList,
	Renderer2,
	TemplateRef,
	ViewChild,
	ViewContainerRef,
	ViewEncapsulation,
} from '@angular/core'
import * as Hammer from 'hammerjs'
import { interval, Observable, Subject, Subscription } from 'rxjs'
import { throttle } from 'rxjs/operators'
import { NgxCarouselItemDirective } from './ngx-light-carousel-item.directive'
import { ngxLightAutoplay, ngxLightOptions } from './ngx-light-carousel.model'

@Component({
	selector: 'ngx-light-carousel',
	templateUrl: './ngx-light-carousel.component.html',
	styleUrls: ['./ngx-light-carousel.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NgxCarouselComponent implements AfterViewInit, OnChanges, OnDestroy {
	@ViewChild('sliderElement', { static: false }) sliderElement: ElementRef
	@ViewChild('prevElement', { static: false }) btnPrevElement: ElementRef
	@ViewChild('nextElement', { static: false }) btnNextElement: ElementRef
	@ViewChild('progressElement', { static: false }) progressContainerElement: ElementRef
	@ContentChildren(NgxCarouselItemDirective, { read: ElementRef, descendants: true }) carouselItems: QueryList<ElementRef>
	@ContentChild('carouselPrev', { static: false }) contentPrev: TemplateRef<any>
	@ContentChild('carouselNext', { static: false }) contentNext: TemplateRef<any>
	@ContentChild('carouselDot', { static: false }) dotElm: TemplateRef<any>
	@ContentChild('carouselProgress', { static: false }) progressElm: TemplateRef<any>
	@ContentChild('infiniteContainer', { static: false, read: ViewContainerRef }) infiniteContainer: ViewContainerRef
	@ContentChild('carouselContent', { static: false }) contentContent: TemplateRef<any>
	@Input() current: number = 0
	@Output() currentChange: EventEmitter<number> = new EventEmitter()
	@Input() data: any[]
	@Input() options: ngxLightOptions
	private infiniteElmRefs: Array<EmbeddedViewRef<any>> = []
	private timeLeft: number = 2000
	private interval
	private actionDelay: Subject<any>
	dots: number[] = []
	grabbing: boolean = false
	throttleSub: Observable<any>
	subRef: Subscription
	arrowsVisible: boolean = true
	carouselItemWidthVar: number

	constructor(@Inject(PLATFORM_ID) private platformId: Object, private _renderer: Renderer2, private cd: ChangeDetectorRef) {
		this.actionDelay = new Subject<any>()
		this.throttleSub = this.actionDelay.asObservable()
		this.throttleSub.pipe(throttle(val => interval(this.options.animation.animationTime + 40))).subscribe(data => {
			data(this)
			this.cd.markForCheck()
		})
	}

	click(fn) {
		this.actionDelay.next(fn)
	}

	ngAfterViewInit() {
		this.init()
		if (this.sufficientSlides()) {
			if (this.infiniteMode()) {
				this.addInfiniteElm()
			}
			if (this.autoplayMode()) {
				this.startTimer()
			}
		}
		this.goToIndex(this.current)
		this.setViewWidth()
		this.calculateDots()
	}

	ngOnChanges() {
		if (this.elements()) {
			this.onResize()
		}
	}

	ngOnDestroy() {
		this.pauseTimer()
		this.subRef.unsubscribe()
	}

	@HostListener('window:resize')
	onResize() {
		this.setViewWidth()
		this.calculateDots()
		if (this.sufficientSlides()) {
			if (this.infiniteMode()) {
				this.addInfiniteElm()
			}
		}
		this.goToIndex(this.currentIndex())
	}

	sufficientSlides() {
		this.showArrows(this.data.length > this.currentBreakpoint().number)
		return this.data.length > this.currentBreakpoint().number
	}

	showArrows(state: boolean) {
		this.arrowsVisible = state
	}

	rootElmWidth() {
		return isPlatformBrowser(this.platformId) ? this.elements().rootElement.getBoundingClientRect().width : 100
	}

	carouselItemWidth() {
		if (this.data.length > this.currentBreakpoint().number) {
			return (this.carouselItemWidthVar = this.rootElmWidth() / this.currentBreakpoint().number)
		} else {
			return (this.carouselItemWidthVar = this.rootElmWidth() / this.data.length)
		}
	}

	currentBreakpoint() {
		for (let i = 0; i < this.options.breakpoints.length; i++) {
			if (this.options.breakpoints[i].width > this.rootElmWidth()) {
				return this.options.breakpoints[i]
			}
		}
	}

	elements() {
		if (this.sliderElement) {
			return {
				rootElement: this.sliderElement.nativeElement,
				containerElement: this.sliderElement.nativeElement.children[0] as HTMLElement,
				carouselItems: this.carouselItems.toArray().map(x => x.nativeElement),
			}
		}
		return
	}

	private setViewWidth() {
		this._renderer.setStyle(this.elements().containerElement, 'display', 'inline-flex')
		this._renderer.setStyle(this.elements().containerElement, 'position', 'relative')
		this.elements().carouselItems.forEach((element: HTMLElement) => {
			this._renderer.addClass(element, 'slide')
			this._renderer.setStyle(element, 'width', this.carouselItemWidth() + 'px')
			this._renderer.setStyle(element, 'display', 'inline-flex')
		})
		if (this.infiniteMode()) {
			this.infiniteElmRefs.forEach(ref => {
				this._renderer.addClass(ref.rootNodes[0], 'slide')
				this._renderer.setStyle(ref.rootNodes[0], 'width', this.carouselItemWidth() + 'px')
			})
		}
	}

	@HostListener('mouseover')
	hover() {
		if (this.autoplayMode()) {
			if ((this.autoplayMode() as any).stopOnHover) {
				this.pauseTimer()
			}
		}
	}

	@HostListener('mouseout')
	unhover() {
		if (this.autoplayMode()) {
			this.pauseTimer()
			this.startTimer()
		}
	}

	private calculateDots() {
		this.dots = []
		if (this.infiniteMode()) {
			var number = this.data.length
		} else {
			var number = this.data.length - this.currentBreakpoint().number + 1
		}
		let results
		if (number > 1) {
			let results = Array.from(Array(Number(number.toFixed(0))).keys())
			this.dots = results
		} else {
			results = []
			this.dots = []
		}
		this.cd.markForCheck()

		return results
	}

	addPanningClass() {
		this.grabbing = true
		this._renderer.addClass(this.elements().rootElement, 'grabbing')
		this._renderer.addClass(this.elements().containerElement, 'panning')
	}

	removePanningClass() {
		this.grabbing = false
		this._renderer.removeClass(this.elements().rootElement, 'grabbing')
		this._renderer.removeClass(this.elements().containerElement, 'panning')
	}

	addAnimationClass() {
		if (this.options?.animation?.animationClass !== '') {
			this._renderer.addClass(this.elements().containerElement, this.options.animation.animationClass)
		}
	}

	removeAnimationClass() {
		if (this.options.animation.animationClass !== '') {
			this._renderer.removeClass(this.elements().containerElement, this.options.animation.animationClass)
		}
	}

	panTo(value: number) {
		if (isPlatformBrowser(this.platformId)) {
			this._renderer.setStyle(this.elements().containerElement, 'transform', `translate3d(${value}px, 0,0)`)
		} else {
			this._renderer.setStyle(this.elements().containerElement, 'transform', `translate3d(${value}%, 0,0)`)
		}
	}

	currentIndex(number?: number) {
		if (!this.infiniteMode() && number !== undefined) {
			if (number >= 0) {
				if (number < 0) {
					this.current = 0
				} else if (number > this.data.length - this.currentBreakpoint().number) {
					this.current = this.data.length - this.currentBreakpoint().number
				} else {
					this.current = number
				}
			}
		} else if (number != undefined) {
			this.current = number
		}
		this.currentChange.emit(number)
		this.cd.markForCheck()

		return this.current
	}

	slowOnOutOfBounds(e) {
		if (!this.options.infinite && this.outOfBound()) {
			e.deltaX *= 0.2
			e.deltaY *= 0.2
		}
		return e
	}

	scrollBackToEdge() {
		const root = this.elements().rootElement.getBoundingClientRect()
		const container = this.elements().containerElement.getBoundingClientRect()
		if (container.right < root.right) {
			this.goToIndex(this.visibleElements())
		}
		if (root.left - container.left < 0) {
			this.goToIndex(0)
		}
	}

	outOfBound() {
		const root = this.elements().rootElement.getBoundingClientRect()
		const container = this.elements().containerElement.getBoundingClientRect()
		if (root.right - container.right > 0) {
			this.currentIndex(this.data.length - 1)
			return true
		}
		if (root.left - container.left < 0) {
			this.currentIndex(0)
			return true
		}
		return false
	}

	detectSwipingLeft(e) {
		return e.velocityX < -this.options.swipe.swipeVelocity
	}

	detectSwipingRight(e) {
		return e.velocityX > this.options.swipe.swipeVelocity
	}

	detectPanningLeft(e) {
		return e.velocityX < 1 && e.distance < -this.carouselItemWidthVar * 1.5
	}

	detectPanningRight(e) {
		return e.velocityX > 1 && e.distance < this.carouselItemWidthVar * 1.5
	}

	visibleElements() {
		return this.data.length - this.currentBreakpoint().number
	}

	nextSlide(that = this) {
		that.addAnimationClass()
		if (that.data.length > 1) {
			if (!that.infiniteMode() && that.visibleElements() < that.currentIndex()) {
				that.goToIndex(that.visibleElements())
			} else {
				that.goToIndex(that.currentIndex() + 1)
			}
			that.infiniteHandler()
		}
		setTimeout(() => {
			that.removeAnimationClass()
		}, that.options.animation.animationTime)
	}

	prevSlide(that = this) {
		that.addAnimationClass()

		if (that.data.length > 1) {
			if (!that.infiniteMode() && 0 >= that.currentIndex()) {
				that.goToIndex(that.currentIndex(0))
			} else {
				that.goToIndex(that.currentIndex() - 1)
			}
			that.infiniteHandler()
		}
		setTimeout(() => {
			that.removeAnimationClass()
		}, that.options.animation.animationTime)
	}

	addInfiniteElm() {
		this.infiniteElmRefs = []
		this.infiniteContainer.clear()
		for (let i = 1; i <= this.data.length; i++) {
			const elm = this.infiniteContainer.createEmbeddedView(this.contentContent, {
				$implicit: this.data[this.data.length - i],
				index: this.data.length - i,
			})
			this._renderer.setStyle(elm.rootNodes[0], 'position', 'absolute')
			this._renderer.setStyle(elm.rootNodes[0], 'transform', `translate3d(-${100 * i}%, 0,0)`)
			this._renderer.setStyle(elm.rootNodes[0], 'display', 'inline-flex')
			this._renderer.setStyle(elm.rootNodes[0], 'visibility', 'visible')
			this._renderer.setStyle(elm.rootNodes[0], 'width', this.carouselItemWidthVar + 'px')
			this._renderer.addClass(elm.rootNodes[0], 'slide')

			const elm2 = this.infiniteContainer.createEmbeddedView(this.contentContent, {
				$implicit: this.data[i - 1],
				index: i - 1,
			})
			this._renderer.setStyle(elm2.rootNodes[0], 'position', 'absolute')
			this._renderer.setStyle(elm2.rootNodes[0], 'right', 0)
			this._renderer.setStyle(elm2.rootNodes[0], 'top', 0)
			this._renderer.setStyle(elm2.rootNodes[0], 'transform', `translate3d(${100 * i}%, 0,0)`)
			this._renderer.setStyle(elm2.rootNodes[0], 'display', 'inline-flex')
			this._renderer.setStyle(elm2.rootNodes[0], 'visibility', 'visible')
			this._renderer.setStyle(elm2.rootNodes[0], 'width', this.carouselItemWidthVar + 'px')
			this._renderer.addClass(elm2.rootNodes[0], 'slide')

			elm.detectChanges()
			elm2.detectChanges()

			this.infiniteElmRefs.push(elm)
			this.infiniteElmRefs.push(elm2)
		}
	}

	private infiniteHandler() {
		// this.removePanningClass()
		this.addAnimationClass()
		setTimeout(() => {
			this.goToIndex(this.currentIndex())
			this.removeAnimationClass()
		}, this.options.animation.animationTime)
		if (0 > this.currentIndex()) {
			this.currentIndex(this.data.length - 1)
			setTimeout(() => {
				this.removeAnimationClass()
				this.goToIndex(this.currentIndex(this.data.length - 1))
			}, this.options.animation.animationTime + 10)
		} else if (this.data.length - 1 < this.currentIndex()) {
			this.currentIndex(0)
			setTimeout(() => {
				this.removeAnimationClass()
				this.goToIndex(this.currentIndex(0))
			}, this.options.animation.animationTime + 10)
		}
		return
	}

	startTimer() {
		var autoplay = this.autoplayMode() as ngxLightAutoplay
		if (autoplay && autoplay.enabled) {
			this.interval = setInterval(() => {
				if (this.timeLeft > 0) {
					this.timeLeft = this.timeLeft - 100
				} else {
					this.timeLeft = JSON.parse(JSON.stringify(autoplay.delay))
					this.actionDelay.next(autoplay.direction == 'left' ? this.prevSlide : this.nextSlide)
				}
			}, 100)
		}
	}

	pauseTimer() {
		try {
			var autoplay = this.autoplayMode() as ngxLightAutoplay
			if (autoplay && autoplay.enabled) {
				this.timeLeft = JSON.parse(JSON.stringify(autoplay.delay))
				clearInterval(this.interval)
			}
			this.timeLeft = JSON.parse(JSON.stringify(autoplay.delay))
			delete this.interval
		} catch {}
	}

	scrollToIndex(number) {
		this.addAnimationClass()
		this.goToIndex(number)
	}

	goToIndex = number => {
		if (this.infiniteMode()) {
			this.panTo(-this.carouselItemWidth() * number)
		} else {
			if (number > -1 && number <= this.data.length - this.currentBreakpoint().number) {
				this.currentIndex(number)
				this.panTo(-this.carouselItemWidth() * number)
			}
			if (number <= -1) {
				this.currentIndex(0)
				this.goToIndex(0)
			}
		}
		this.currentIndex(number)
	}

	infiniteMode() {
		return this.options.infinite
	}

	autoplayMode() {
		if (this.options.autoplay && this.options.autoplay.enabled) {
			return this.options.autoplay
		} else {
			return false
		}
	}

	calculateMoved(e) {
		const moved = Math.round(Math.abs(e.deltaX) / this.carouselItemWidth())
		return moved > 0 ? moved : 1
	}

	moveIndex(move: number) {
		return this.currentIndex() + move
	}

	init() {
		if (this.data.length > 1 && this.sufficientSlides()) {
			var sliderManager = new Hammer.Manager(this.elements().containerElement)
			sliderManager.add(new Hammer.Pan({ threshold: 0, pointers: 0 }))
			sliderManager.on('panleft panright panend pancancel', (e: Hammer.HammerInput) => {
				switch (e.type) {
					case 'panleft':
					case 'panright':
						this.pauseTimer()
						if (!this.grabbing) {
							this.removeAnimationClass()
							this.addPanningClass()
						}
						if (this.options.drag.draggable) {
							this.panTo(-this.current * this.carouselItemWidthVar + 0 + e.deltaX)
						}
						break
					case 'pancancel':
						this.removePanningClass()
						this.addAnimationClass()
						this.goToIndex(this.currentIndex())
						break
					case 'panend':
						this.pauseTimer()
						this.removePanningClass()
						this.addAnimationClass()

						let moved = this.calculateMoved(e)
						if (!this.infiniteMode()) {
							if (this.outOfBound()) {
								this.scrollBackToEdge()
								setTimeout(() => {
									this.removeAnimationClass()
								}, this.options.animation.animationTime)
								break
							}
						}

						if (this.options.drag.draggable) {
							if (this.detectPanningLeft(e)) {
								this.addAnimationClass()
								this.goToIndex(this.moveIndex(+moved))
								this.infiniteHandler()
								setTimeout(() => {
									this.removeAnimationClass()
								}, this.options.animation.animationTime)

								break
							}
							if (this.detectPanningRight(e)) {
								this.addAnimationClass()
								if (1.00000001 < this.currentIndex() && this.currentIndex() < 1.999999999999) {
									this.goToIndex(Math.ceil(this.moveIndex(-moved)))
								} else {
									this.goToIndex(this.moveIndex(-moved))
								}
								this.infiniteHandler()
								setTimeout(() => {
									this.removeAnimationClass()
								}, this.options.animation.animationTime)

								break
							}
						}
						if (this.options.swipe.swipeable) {
							if (this.detectSwipingLeft(e)) {
								this.nextSlide(this)
								this.infiniteHandler()
								if (!this.infiniteMode()) {
									this.addAnimationClass()
									if (0.00000001 < this.currentIndex() && this.currentIndex() < 0.999999999999) {
										this.removeAnimationClass()
										this.goToIndex(0)
									}
								}
								break
							}
							if (this.detectSwipingRight(e)) {
								this.prevSlide()
								this.infiniteHandler()
								if (!this.infiniteMode()) {
									if (0.00000001 < this.currentIndex() && this.currentIndex() < 0.999999999999) {
										this.addAnimationClass()
										this.goToIndex(0)
										this.removeAnimationClass()
									}
								}
								break
							}
						}
				}
			})
		}
	}
}
