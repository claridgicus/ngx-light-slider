# NGX Light Carousel

Hi! This is the NGX Light Carousel package - a light carousel crafted in angular with HammerJS for touch support

This is a fully responsive, highly configurable carousel / slider designed for ngx currently with support for 8, but it should work on previous versions of Angular

Please note this is a link to a fully working repo if you'd like to take it and do you're own thing with it.

Please log any issues you enounter to the gitlab board, I personally use this on a number of websites so it's under active development.

Contributions are welcome via PR.

# Demo

[Click me!](https://ngx-light-carousel.stackblitz.io)

# Example Sandbox

[Click me!](https://stackblitz.com/edit/ngx-light-carousel?file=src%2Fapp%2Fapp.component.html)

# Carousel Options

|NGX Light Carousel| Supported | Notes |
|------------------|-----------|----------|
|Responsive		   |✔️         | Configure as many breakpoints as you like
|AutoPlay          |✔️         | Autoplay with customizable time, pause on hover and direction
|Carousel Index Dots|✔️         |  Optional Index Dots, clickable with active state
|Touch Gestures    |✔️         | HammerJS is used for touch support, responsiveness is configurable
|- Swipeable |✔️         |  Configure swiping to the next or previous item
|- Pannable |✔️         |  Configure panning through items, with a outofbounds springback
|Infinite Loop    |✔️         | Appends sudo items before and after for infinite looping capabilities
|Accepts *ngFor HTML items    |✔️         | Carousel accepts a directive tagged *ngFor, Flexible AF
|Configurable Animation    |✔️         | Animation is standardised, but can be customised by overriding css and specifing new duration
|Fully Typed Config Objects |✔️         | Our configs are fully typed for your convenience 

# Installation

    # Install ngx Light Carousel

    npm i --save ngx-light-carousel@latest
    
   
    # Include it in the app module
    
    @NgModule({
	    declarations: [AppComponent],
	    imports: [BrowserModule, NgxCarouselModule],
	    bootstrap: [AppComponent]
    })
    


    # Include the HTML for your Carousel on the component you wish to display it on
    # yourcomponentname.component.html
    
	    <ngx-light-carousel  [options]="options"  [data]="slides">
		    <section  ngx-light-carousel-container>
			    <article  ngx-light-carousel-item  *ngFor="let slide of slides; let i = index">
				    <div class="slide w-100">
                        <a href="{{product.url}}">
                            <img class="w-100" src="{{product.image}}">
                        </a>
                        <h3 class="title">{{product.title}}</h3>
                    </div>
				 </article>
				 <!-- include the outlet for infinite scroll at the end of the loop -->
				 <ng-template  #infiniteContainer></ng-template> 
			</section>
			
			<!-- define what the infinite scroll item looks like, typically a copy of the regular item -->
			<ng-template #carouselContent let-slide let-i="index">
			<article>
                <div class="slide w-100">
                    <a href="{{product.url}}">
                        <img class="w-100" src="{{product.image}}">
                    </a>
                    <h3 class="title">{{product.title}}</h3>
                </div>
			</article>
			</ng-template>
			
			<!-- Custom contents for the previous button -->
			<ng-template  #carouselPrev>
				<div  class="click-area">
				</div>
			</ng-template>

			<!-- Custom contents for the next button -->
			<ng-template  #carouselNext>
				<div  class="click-area">
				</div>
			</ng-template>
			
			<!-- Custom contents for the carousel index -->
			<ng-template  #carouselDot  let-model>
				<div class="ball bg-accent">
				</div>
			</ng-template>
			
			<!-- Custom contents for the progress bar (todo) -->
			<ng-template  #carouselProgress  let-progress>
				<div  class="progress"></div>
			</ng-template>
		</ngx-light-carousel>



	# Construct a config object & slides to provide to the carousel
	# most have sane defaults if you leave them blank, some are required
	# If your data is async then add an *ngIf to the carousel element as you would normally
	# yourcomponentname.component.ts
	
		import { ngxLightOptions } from  'ngx-light-carousel/public-api'
		~~~~~~~
		various Angular component decorators etc
        then
        export class YourComponentNameComponent implements OnInit {
        ~~~~~~~
		options: ngxLightOptions
		slides: any[]		

		constructor(){
			// construct the config object
			this.options = {
					animation: {
						animationClass: 'transition',
						animationTime: 200,
					},
					swipe: {
						swipeable: true,
						swipeVelocity: 1,
					},
					drag: {
						draggable: true,
						dragMany: true,
					},
					infinite: true,
					autoplay: {
						enabled: true,
						direction: 'right',
						delay: 5000,
						stopOnHover: true,
					},
					breakpoints: [
						{
							width: 768,
							number: 1,
						},
						{
							width: 991,
							number: 3,
						},
						{
							width: 9999,
							number: 4,
						},
					],
				}
				
				// construct an array of slides
                this.slides = []
				this.slides.push({
					title: 'RED Widgets',
					url: 'https://url',
					image: `http://picsum.photos/600/400/?0`,
				})
				this.slides.push({
					title: 'YELLOW Widgets',
					url: 'https://url',
					image: `http://picsum.photos/600/400/?1`,
				})
				this.slides.push({
					title: 'Black Widgets',
					url: 'https://url',
					image: `http://picsum.photos/600/400/?2`,
				})
				this.slides.push({
					title: 'Grey Widgets',
					url: 'https://url',
					image: `http://picsum.photos/600/400/?3`,
				})
				this.slides.push({
					title: 'Green Widgets',
					url: 'https://url',
					image: `http://picsum.photos/600/400/?4`,
				})
				
			}
	
	

   


# Carousel Configuration Object
   An Example Configuration Object:
    
    options: {
	    animation: {
		    animationClass:  string
		    animationTime:  number
	    }
	    swipe: {
		    swipeable:  boolean
		    swipeVelocity:  number
	    }
	    drag: {
		    draggable:  boolean
		    dragMany:  boolean
	    }
	    scroll: {
		    numberToScroll:  number
	    }
	    infinite:  boolean
	    autoplay: {
		    enabled:  true,
		    direction:  'right',
		    delay:  5000,
		    stopOnHover:  true,
	    },
	    breakpoints: [
		    {
			    width:  768,  // the max resoloution for
			    number:  1, // how many slides are visible at this breakpoint
		    }
		]
	}
	    
    