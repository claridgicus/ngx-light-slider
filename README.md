# NGX Light Fader

Hi! This is the NGX Light Fader package - a light fader crafted in angular with HammerJS for touch support

This is a fully responsive, highly configurable carousel / slider designed for ngx currently with support for 8, but it should work on previous versions of Angular


# Carousel Options

|NGX Light Carousel| Supported | Notes |
|------------------|-----------|----------|
|Responsive		   |✔️         | 
|AutoPlay          |✔️         | Autoplay with customizable time, pause on hover and direction
|Carousel Index Dots|✔️         |  Optional Index Dots, clickable with active state
|Touch Gestures    |✔️         | HammerJS is used for touch support, responsiveness is configurable
|- Swipeable |✔️         |  Configure swiping to the next or previous item
|Infinite Loop    |✔️         | Appends sudo items before and after for infinite looping capabilities
|Accepts *ngFor HTML items    |✔️         | Carousel accepts a directive tagged *ngFor, Flexible AF
|Configurable Animation    |✔️         | Animation is standardised, but can be customised by overriding css and specifing new duration

# Installation

    # Install ngx Light Carousel

    npm i --save ngx-light-carousel@latest
    
   
    # Include it in the app moduel
    
    @NgModule({
	    declarations: [AppComponent],
	    imports: [BrowserModule, NgxFaderModule],
	    bootstrap: [AppComponent]
    })
    


    # Include the HTML for your Fader on the component you wish to display it on
    # yourcomponentname.component.html
    
	    <ngx-light-fader  [options]="options"  [data]="slides">
		    <section  ngx-light-fader-container>
			    <article  ngx-light-fader-item  *ngFor="let slide of slides; let i = index">
				    <div class="item cursor-pointer">
				       //example item
				    </div>
				 </article>
				 <!-- include the outlet for infinite scroll at the end of the loop -->
				 <ng-template  #infiniteContainer></ng-template> 
			</section>
			
			<!-- define what the infinite scroll item looks like, typically a copy of the regular item -->
			<ng-template #faderContent let-slide let-i="index">
			<article>
				 <div class="item cursor-pointer">
				       //example item
				</div>
			</article>
			</ng-template>
			
			<!-- Custom contents for the previous button -->
			<ng-template  #faderPrev>
				<div  class="click-area">
				</div>
			</ng-template>

			<!-- Custom contents for the next button -->
			<ng-template  #faderNext>
				<div  class="click-area">
				</div>
			</ng-template>
			
			<!-- Custom contents for the fader index -->
			<ng-template  #faderDot  let-model>
				<div class="ball bg-accent">
				</div>
			</ng-template>
			
			<!-- Custom contents for the progress bar (todo) -->
			<ng-template  #faderProgress  let-progress>
				<div  class="progress"></div>
			</ng-template>
		</ngx-light-fader>



	# Construct a config object & slides to provide to the carousel
	# most have sane defaults if you leave them blank, some are required
	# If your data is async then add an *ngIf to the carousel element as you would normally
	# yourcomponentname.component.ts
	
		import { ngxLightOptions } from  'ngx-light-fader'
		~~~~~~~
		Angular component decorators etc
		~~~~~~~
		options: ngxLightFaderOptions
		slides: any[]		

		constructor(){
			// construct the config object
			this.options = {
					animation: {
						animationClass: 'transition',
						animationTime: 200,
					},
					swipe: {
						swipeable: true,
						swipeVelocity: 1,
					},
					drag: {
						draggable: true,
						dragMany: true,
					},
					infinite: true,
					autoplay: {
						enabled: true,
						direction: 'right',
						delay: 5000,
						stopOnHover: true,
					},
					breakpoints: [
						{
							width: 768,
							number: 1,
						},
						{
							width: 991,
							number: 3,
						},
						{
							width: 9999,
							number: 4,
						},
					],
				}
				
				// construct an array of slides
				this.slides.push({
					title: 'RED Widgets',
					url: 'https://url',
					regularPrice: '100.00',
					salePrice: '100.00',
					image: `http://picsum.photos/600/400/?0`,
				})
				this.slides.push({
					title: 'YELLOW Widgets',
					url: 'https://url',
					regularPrice: '100.00',
					salePrice: '100.00',
					image: `http://picsum.photos/600/400/?1`,
				})
				this.slides.push({
					title: 'Black Widgets',
					url: 'https://url',
					regularPrice: '100.00',
					salePrice: '100.00',
					image: `http://picsum.photos/600/400/?2`,
				})
				this.slides.push({
					title: 'Black Widgets',
					url: 'https://url',
					regularPrice: '100.00',
					image: `http://picsum.photos/600/400/?3`,
				})
				this.slides.push({
					title: 'Black Widgets',
					url: 'https://url',
					regularPrice: '100.00',
					image: `http://picsum.photos/600/400/?4`,
				})
				
			}
	
	

   


# Carousel Configuration Object
   An Example Configuration Object:
    
    options: {
	    animation: {
		    animationClass:  string
		    animationTime:  number
	    }
	    swipe: {
		    swipeable:  boolean
		    swipeVelocity:  number
	    }
	    drag: {
		    draggable:  boolean
		    dragMany:  boolean
	    }
	    scroll: {
		    numberToScroll:  number
	    }
	    infinite:  boolean
	    autoplay: {
		    enabled:  true,
		    direction:  'right',
		    delay:  5000,
		    stopOnHover:  true,
	    },
	    breakpoints: [
		    {
			    width:  768,  // the max resoloution for
			    number:  1, // how many slides are visible at this breakpoint
		    }
		]
	}
	    
    